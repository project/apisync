<?php

declare(strict_types=1);

namespace Drupal\apisync;

use Drupal\apisync\Entity\ApiSyncAuthConfigInterface;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Auth provider plugin manager interface.
 */
interface ApiSyncAuthProviderPluginManagerInterface extends PluginManagerInterface, FallbackPluginManagerInterface {

  /**
   * Get a list of all the auth providers.
   *
   * @return \Drupal\apisync\Entity\ApiSyncAuthConfigInterface[]
   *   An array of all providers in the auth storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getProviders(): array;

  /**
   * Check if there are any auth providers in the auth storage.
   *
   * @return bool
   *   TRUE if the auth storage contains providers.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function hasProviders(): bool;

  /**
   * Get the auth config entity.
   *
   * @return \Drupal\apisync\Entity\ApiSyncAuthConfigInterface|null
   *   The auth config, or NULL.
   */
  public function getConfig(): ?ApiSyncAuthConfigInterface;

  /**
   * Get the configured auth provider plugin.
   *
   * @return \Drupal\apisync\ApiSyncAuthProviderInterface|null
   *   The auth provider plugin, or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getProvider(): ?ApiSyncAuthProviderInterface;

  /**
   * Provides an existing or new token.
   *
   * @return string|null
   *   The OAuth token, or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getAccessToken(): string|null;

  /**
   * Refresh the OAuth token.
   *
   * @return string|null
   *   The OAuth token, or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function refreshAccessToken(): string|null;

  /**
   * Append the auth headers.
   *
   * @param array $headers
   *   The headers to be appended with the authentication headers.
   *
   * @return array
   *   The headers including the authentication headers.
   *
   * @throws \Drupal\apisync\Exception\IdentityNotFoundException
   *   Thrown when the auth provider cannot provide the authentication headers.
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function appendAuthHeaders(array $headers): array;

}
