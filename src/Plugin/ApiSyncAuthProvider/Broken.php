<?php

declare(strict_types=1);

namespace Drupal\apisync\Plugin\ApiSyncAuthProvider;

use Drupal\apisync\ApiSyncAuthProviderPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Fallback for broken / missing plugin.
 *
 * @Plugin(
 *   id = "broken",
 *   label = @Translation("Broken or missing provider"),
 * )
 */
class Broken extends ApiSyncAuthProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function refreshAccessToken(): string|null {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    $this->messenger()->addError($this->t('Auth provider for %id is missing or broken.', ['%id' => $this->id]));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(): string|null {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccessToken(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function clearAccessToken(): void {
    // Noop.
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $formState): void {
    $this->messenger()->addError($this->t('Auth provider for %id is missing or broken.', ['%id' => $this->id]));
  }

  /**
   * {@inheritdoc}
   */
  public function appendAuthHeaders(array $headers = []): array {
    return $headers;
  }

  /**
   * {@inheritdoc}
   */
  public function isTokenBasedProvider(): bool {
    return FALSE;
  }

}
