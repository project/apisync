<?php

declare(strict_types=1);

namespace Drupal\apisync\Exception;

/**
 * An exception when using the API.
 */
class Exception extends \RuntimeException {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $message = "",
    $code = 0,
    $previous = NULL,
  ) {
    // Prevent loggers from having issues.
    $encodedMessage = mb_convert_encoding($message, 'UTF-8');
    parent::__construct($encodedMessage, $code, $previous);
  }

}
