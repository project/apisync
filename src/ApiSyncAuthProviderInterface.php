<?php

declare(strict_types=1);

namespace Drupal\apisync;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * API Sync Auth Provider Interface.
 */
interface ApiSyncAuthProviderInterface extends PluginFormInterface, ContainerFactoryPluginInterface, PluginInspectionInterface {

  /**
   * ID of this service.
   *
   * @return string
   *   ID of this service.
   */
  public function id(): string;

  /**
   * Label of this service.
   *
   * @return string
   *   Id of this service.
   */
  public function label(): string;

  /**
   * Is the provider token based.
   *
   * @return bool
   *   The provider is token based.
   */
  public function isTokenBasedProvider(): bool;

  /**
   * Retrieve the access token.
   *
   * @return string|null
   *   The access token.
   */
  public function getAccessToken(): string|null;

  /**
   * Check if plugin has acces token.
   *
   * @return bool
   *   TRUE if plugin has a valid access token.
   */
  public function hasAccessToken(): bool;

  /**
   * Refresh the access token.
   *
   * @return string|null
   *   The new access token.
   */
  public function refreshAccessToken(): string|null;

  /**
   * Clear the current access token.
   */
  public function clearAccessToken(): void;

  /**
   * Get the authentication headers.
   *
   * @param array $headers
   *   The headers to be appended with the authentication headers.
   *
   * @return array
   *   The headers including the authentication headers.
   *
   * @throws \Drupal\apisync\Exception\IdentityNotFoundException
   *   Thrown when the auth provider cannot provide the authentication headers.
   */
  public function appendAuthHeaders(array $headers = []): array;

}
