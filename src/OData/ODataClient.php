<?php

declare(strict_types=1);

namespace Drupal\apisync\OData;

use Drupal\apisync\ApiSyncAuthProviderPluginManagerInterface;
use Drupal\apisync\Exception\ApiException;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as ResponseCode;

/**
 * Objects, properties, and methods to communicate with the API Sync REST API.
 *
 * @todo Check where all request exceptions are caught and swap them out for ApiException.
 */
class ODataClient implements ODataClientInterface {

  use StringTranslationTrait;

  /**
   * API Sync immutable config object.  Useful for gets.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $apisyncSettingsConfig;

  protected const CACHE_LIFETIME = 300;

  /**
   * Constructor which initializes the consumer.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The GuzzleHttp Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \Drupal\apisync\ApiSyncAuthProviderPluginManagerInterface $authManager
   *   The auth manager.
   */
  public function __construct(
    protected ClientInterface $httpClient,
    protected ConfigFactoryInterface $configFactory,
    protected StateInterface $state,
    protected CacheBackendInterface $cache,
    protected TimeInterface $time,
    protected ApiSyncAuthProviderPluginManagerInterface $authManager,
  ) {
    $this->apisyncSettingsConfig = $this->configFactory->get('apisync.settings');
  }

  /**
   * Get the short term cache lifetime.
   *
   * @return int
   *   The short term cache lifetime.
   */
  public function getShortTermCacheLifetime(): int {
    return $this->apisyncSettingsConfig->get('short_term_cache_lifetime') ?? static::CACHE_LIFETIME;
  }

  /**
   * Make an API call.
   *
   * @param string $path
   *   The path to request.
   * @param array $params
   *   The parameters to provide.
   * @param string $method
   *   The request method. (GET, POST, PATCH, etc.)
   * @param bool $returnObject
   *   An object will be returned.
   *
   * @return array|\Psr\Http\Message\ResponseInterface
   *   The response data or the response object.
   *
   * @throws \Drupal\apisync\Exception\ApiException
   *   If the request fails.
   */
  protected function apiCall(
    string $path,
    array $params = [],
    string $method = 'GET',
    bool $returnObject = FALSE,
  ): array|ResponseInterface {
    $headers = ['Content-type' => 'application/json'];
    $headers += $method === 'PATCH' ? ['If-Match' => '*'] : [];
    $data = NULL;
    if (!empty($params)) {
      $data = $this->jsonEncode($params);
    }
    $options = [
      'headers' => $headers,
      'body' => $data,
    ];
    $response = $this->request($method, $path, $options);
    if ($returnObject) {
      return new $response();
    }
    return $this->jsonDecode($response->getBody()->getContents());
  }

  /**
   * Json encode data.
   *
   * @param array $data
   *   The data to encode.
   *
   * @return string
   *   The encoded data.
   */
  protected function jsonEncode(array $data): string {
    $json = Json::encode($data);
    if ($json === FALSE) {
      throw new ApiException('Failed to encode data to JSON.');
    }
    return $json;
  }

  /**
   * Json decode data.
   *
   * @param string $data
   *   The data to decode.
   *
   * @return array
   *   The decoded data.
   */
  protected function jsonDecode(string $data): array {
    $json = Json::decode($data);
    if ($json === NULL) {
      throw new ApiException('Failed to decode data from JSON.');
    }
    return $json;
  }

  /**
   * Helper to issue an XML API request.
   *
   * @param string $url
   *   Fully-qualified URL to resource.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response object.
   *
   * @throws \Drupal\apisync\Exception\ApiException
   */
  protected function apiXmlHttpRequest(string $url): ResponseInterface {
    $options = [
      'headers' => ['Content-type' => 'application/xml'],
      'body' => NULL,
    ];
    return $this->request('GET', $url, $options);
  }

  /**
   * Make a http request.
   *
   * Provides a wrapper around the GuzzleHttp\ClientInterface::request() method.
   * This allows refreshing the token if the token is invalid.
   *
   * @param string $method
   *   Method to initiate the call, such as GET or POST.  Defaults to GET.
   * @param string $uri
   *   Fully-qualified URL to resource.
   * @param array $options
   *   Request options.
   * @param bool $retry
   *   If TRUE, the request will be retried if the token is invalid.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response object.
   *
   * @throws \Drupal\apisync\Exception\ApiException
   */
  protected function request(
    string $method,
    string $uri = '',
    array $options = [],
    bool $retry = TRUE,
  ): ResponseInterface {
    try {
      // There is no point retrying if the provider is not token based.
      $retry = $this->authManager->isTokenBasedProvider() && $retry;
      // Make sure the URI is fully qualified.
      if (!str_starts_with($uri, 'http')) {
        $uri = $this->getInstanceUrl() . '/' . ltrim($uri, '/');
      }
      // Makes exception handling easier.
      $options['http_errors'] = TRUE;
      // Avoid hard to debug issues.
      $options['allow_redirects'] = FALSE;
      $options['headers'] = $this->authManager->appendAuthHeaders($options['headers'] ?? []);
      return $this->httpClient->request($method, $uri, $options);
    }
    catch (ClientException $e) {
      // If the auth provider is not token based, then there is nothing we can
      // do to recover from this exception.
      if (!$this->authManager->isTokenBasedProvider()) {
        throw new ApiException('Could not make a request: ' . $e->getMessage());
      }
      // The token maybe completely incorrect, revoked or expired. It is also
      // possible the token has the wrong scope, but getting a new token
      // may help (if we now have a new oauth2 client implementation).
      $forbiddenOrUnauthorized = in_array(
        $e->getResponse()->getStatusCode(),
        [ResponseCode::HTTP_FORBIDDEN, ResponseCode::HTTP_UNAUTHORIZED],
        TRUE
      );
      if ($forbiddenOrUnauthorized) {
        // Ensures that a new token is got with the next request.
        $this->authManager->clearAccessToken();
        if ($retry) {
          return $this->request($method, $uri, $options, FALSE);
        }
      }
      throw new ApiException('Could not make a request ' . $e->getMessage());
    }
    catch (\Throwable $e) {
      // This can be any other exception, such as gateway exceptions.
      throw new ApiException('Failed to make a request: ' . $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function objects(bool $reset = FALSE): array {
    // Use the cached data if we have it.
    $cache = $this->cache->get('odata:objects');
    if (!$reset && $cache) {
      $objects = $cache->data;
    }
    else {
      $url = $this->getMetadataUrl();
      $result = new XMLResponse($this->apiXmlHttpRequest($url));
      $parser = new ODataMetadataParser($result->data);
      $objects = $parser->getSchemaProperties();
      $this->cache->set(
          'odata:objects',
          $objects,
          $this->getRequestTime() + $this->getShortTermCacheLifetime(),
          ['apisync']
      );
    }
    return $objects;
  }

  /**
   * {@inheritdoc}
   *
   * @see apiCall()
   */
  public function query(SelectQueryInterface $query): SelectQueryResultInterface {
    // Casting $query as a string calls SelectQuery::__toString().
    $queryParams = (string) $query;

    return new SelectQueryResult($this->apiCall('/' . $queryParams));
  }

  /**
   * {@inheritdoc}
   *
   * @see apiCall()
   */
  public function queryAll(SelectQueryInterface $query): SelectQueryResultInterface {
    return new SelectQueryResult($this->apiCall('/queryAll?q=' . (string) $query));
  }

  /**
   * {@inheritdoc}
   *
   * @see apiCall()
   */
  public function queryMore(SelectQueryResultInterface $results): SelectQueryResultInterface {
    if ($results->done()) {
      return new SelectQueryResult([
        'totalSize' => $results->size(),
        'done' => TRUE,
        'records' => [],
      ]);
    }
    $fullUrl = urldecode($results->nextRecordsUrl());
    $nextRecordsUrl = str_replace($this->getInstanceUrl(), '', $fullUrl);
    return new SelectQueryResult($this->apiCall($nextRecordsUrl));
  }

  /**
   * {@inheritdoc}
   *
   * @see objects()
   */
  public function objectDescribe(string $name, bool $reset = FALSE): mixed {
    $objects = $this->objects($reset);
    return $objects[$name];
  }

  /**
   * {@inheritdoc}
   *
   * @see apiCall()
   */
  public function objectCreate(string $objectType, array $params): ODataObjectInterface {
    return new ODataObject($this->apiCall("/$objectType", $params, 'POST'));
  }

  /**
   * {@inheritdoc}
   *
   * @see apiCall()
   */
  public function objectUpdate(string $path, array $params): void {
    $this->apiCall($path, $params, 'PATCH');
  }

  /**
   * {@inheritdoc}
   *
   * @see apiCall()
   */
  public function objectRead(string $path): ODataObjectInterface {
    return new ODataObject($this->apiCall($path));
  }

  /**
   * {@inheritdoc}
   *
   * @see apiCall()
   */
  public function objectDelete(string $path, bool $throwException = FALSE): void {
    try {
      $this->apiCall($path, [], 'DELETE');
    }
    catch (RequestException $e) {
      if ($throwException || $e->getResponse()?->getStatusCode() !== 404) {
        throw $e;
      }
    }
  }

  /**
   * Returns REQUEST_TIME.
   *
   * @return int
   *   The REQUEST_TIME server variable.
   */
  protected function getRequestTime(): int {
    return $this->time->getRequestTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceUrl(): string {
    return rtrim($this->apisyncSettingsConfig->get('instance_url'), '/');
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataUrl(): string {
    return rtrim($this->apisyncSettingsConfig->get('metadata_url'), '/');
  }

}
