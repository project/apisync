<?php

declare(strict_types=1);

namespace Drupal\apisync_oauth\Plugin\ApiSyncAuthProvider;

use Drupal\apisync\ApiSyncAuthProviderPluginBase;
use Drupal\apisync\Exception\IdentityNotFoundException;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\oauth2_client\Entity\Oauth2ClientListBuilder;
use Drupal\oauth2_client\Exception\InvalidOauth2ClientException;
use Drupal\oauth2_client\Service\Oauth2ClientServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Basic Auth plugin.
 *
 * @Plugin(
 *   id = "apisync_oauth",
 *   label = @Translation("oAuth2 Authentication")
 * )
 */
class ApiSyncOAuthPlugin extends ApiSyncAuthProviderPluginBase {

  use StringTranslationTrait;

  /**
   * ApiSyncBasicAuthPlugin Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   Plugin id.
   * @param array $pluginDefinition
   *   Plugin definition.
   * @param \Drupal\oauth2_client\Service\Oauth2ClientServiceInterface $oauth2ClientService
   *   The OAuth2 client service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $pluginDefinition,
    protected Oauth2ClientServiceInterface $oauth2ClientService,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ): static {
    $configuration = array_merge(static::defaultConfiguration(), $configuration);
    $clientService = $container->get('oauth2_client.service');
    assert($clientService instanceof Oauth2ClientServiceInterface);
    // Accept this common Drupal pattern.
    // @phpstan-ignore-next-line
    return new static(
        $configuration,
        $pluginId,
        $pluginDefinition,
        $clientService,
        $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultConfiguration(): array {
    return [
      'client_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    $options = $this->getOauth2ClientOptions();
    // @todo Create a default oauth2 client that has configurable settings.
    if (empty($options)) {
      $form['no_clients'] = [
        '#markup' => $this->t('No oAuth2 client is available. Please create one first. This requires a developer to create a plugin instance.'),
      ];
      return $form;
    }
    $form['client_id'] = [
      '#title' => $this->t('Client ID'),
      '#type' => 'select',
      '#options' => $options,
      '#description' => $this->t('oAuth2 client ID'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['client_id'],
    ];
    return $form;
  }

  /**
   * Get a list of available oAuth2 clients.
   *
   * @return array
   *   An array of client options.
   */
  protected function getOauth2ClientOptions(): array {
    try {
      $listBuilder = $this->entityTypeManager->getHandler(
        entity_type_id: 'oauth2_client',
        handler_type: 'list_builder'
      );
      assert($listBuilder instanceof Oauth2ClientListBuilder);
      return array_reduce(
        $listBuilder->load(),
        static function ($carry, $client) {
          $carry[$client->id()] = $client->label();
          return $carry;
        },
        []
      );
    }
    catch (InvalidPLuginDefinitionException $e) {
      return [];
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(): string|null {
    // The current implementation of the Oauth2ClientService handles the
    // exception, but the interface indicates that an exception could be thrown.
    // Therefore, we wrap the call in a try-catch block.
    try {
      return $this->oauth2ClientService->getAccessToken(
        $this->configuration['client_id'],
        NULL
      )?->getToken();
    }
    catch (InvalidOauth2ClientException $e) {
      // The exception is logged by the Oauth2ClientService.
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccessToken(): bool {
    // Returns an existing or new token (only if needed) and so is relatively
    // inexpensive.
    return !empty($this->getAccessToken());
  }

  /**
   * {@inheritdoc}
   */
  public function clearAccessToken(): void {
    // The oauth2 does not support revoking a token out of the box.
    // Therefore, we just clear the token, which removes this client's access.
    // However, any other client will continue to be able to use the token.
    // The current implementation of the Oauth2ClientService handles the
    // exception, but the interface indicates that an exception could be thrown.
    // Therefore, we wrap the call in a try-catch block.
    try {
      $this->oauth2ClientService->clearAccessToken($this->configuration['client_id']);
    }
    catch (InvalidOauth2ClientException $e) {
      // The exception is logged by the Oauth2ClientService.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $formState): void {
    // Check that the chosen client is properly configured and active.
    $providerSettings = $formState->getValue('provider_settings');
    $clientId = $providerSettings['client_id'];
    if (empty($clientId)) {
      $formState->setErrorByName('client_id', $this->t('Please select a client.'));
      return;
    }
    $token = NULL;
    $message = $this->t('It was not possible to get a token for the client.');
    try {
      // We first clear token to force a new token to be generated. This
      // allows us to test that the client is currently active and configured
      // correctly.
      $this->oauth2ClientService->clearAccessToken($clientId);
      $token = $this->oauth2ClientService->getAccessToken($clientId, NULL)?->getToken();
      $this->messenger()->addStatus($this->t('Token retrieved successfully.'));
    }
    catch (InvalidOauth2ClientException $e) {
      // The exception is logged by the current Oauth2ClientService and not
      // rethrown, so we should never actually get here. We don't know if there
      // will be other implementations that thrown the exception in the future,
      // so we include this catch block.
      $formState->setErrorByName('client_id', $message);
    }
    if (empty($token)) {
      $formState->setErrorByName('client_id', $message);
    }
    parent::validateConfigurationForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function isTokenBasedProvider(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function appendAuthHeaders(array $headers = []): array {
    $accessToken = $this->getAccessToken();
    if (empty($accessToken)) {
      throw new IdentityNotFoundException('oAuth2 token is missing.');
    }
    return $headers + [
      'Authorization' => 'Bearer ' . $this->getAccessToken(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function refreshAccessToken(): string|null {
    // OAuth implementations don't always support refreshing the
    // token with grant type of client credentials. This is really not needed
    // as we can always get a new token with the credentials rather than
    // refreshing the token. We force getting a new token by clearing the
    // existing token and getting a new one. Revoking here means that we just
    // clear the token from storage and don't actually revoke it.
    $this->clearAccessToken();
    return $this->getAccessToken();
  }

}
