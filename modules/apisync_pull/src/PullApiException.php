<?php

declare(strict_types=1);

namespace Drupal\apisync_pull;

use Drupal\apisync\Exception\ApiException;

/**
 * Pull exception.
 */
class PullApiException extends ApiException {

}
