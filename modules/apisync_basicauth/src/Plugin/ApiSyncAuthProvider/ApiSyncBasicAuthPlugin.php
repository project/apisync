<?php

declare(strict_types=1);

namespace Drupal\apisync_basicauth\Plugin\ApiSyncAuthProvider;

use Drupal\apisync\ApiSyncAuthProviderPluginBase;
use Drupal\apisync\Exception\IdentityNotFoundException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Basic Auth plugin.
 *
 * @Plugin(
 *   id = "basic_auth",
 *   label = @Translation("Basic Authentication")
 * )
 */
class ApiSyncBasicAuthPlugin extends ApiSyncAuthProviderPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultConfiguration(): array {
    return [
      'login_user' => '',
      'login_password' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {

    $form['login_user'] = [
      '#title' => $this->t('Username'),
      '#type' => 'textfield',
      '#description' => $this->t('Username'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['login_user'],
    ];

    $form['login_password'] = [
      '#title' => 'Password',
      '#type' => 'password',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function appendAuthHeaders(array $headers = []): array {
    if (empty($this->configuration['login_user'] || empty($this->configuration['login_password']))) {
      throw new IdentityNotFoundException('Basic Auth credentials are missing.');
    }
    return $headers + [
      'Authorization' => 'Basic ' . base64_encode($this->configuration['login_user'] . ':' . $this->configuration['login_password']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isTokenBasedProvider(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(): string|null {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccessToken(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function clearAccessToken(): void {
    // No operation as not token based.
  }

  /**
   * {@inheritdoc}
   */
  public function refreshAccessToken(): string|null {
    return NULL;
  }

}
