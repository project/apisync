<?php

declare(strict_types=1);

namespace Drupal\apisync\Exception;

/**
 * An exception when using the API.
 */
class ApiException extends Exception {
}
